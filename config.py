bot_id = "@mtrxdirect:vern.cc"
hs_url = "https://mtrx.vern.cc"
rooms = "all"
password = input("Enter your bot account's password: (Note: the password isnt hidden, i.e not represented with *****) ")
storage_path="/home/dev/mtrxdirect/client_storage"
purify_db = {
    "youtube.com" : "inv.vern.cc",
    "www.youtube.com" : "inv.vern.cc",
    "reddit.com" : "lr.vern.cc",
    "www.reddit.com" : "lr.vern.cc",
    "imgur.com" : "rimgo.vern.cc", 
    "www.imgur.com" : "rimgo.vern.cc", 
    "i.imgur.com" : "rimgo.vern.cc",
    "odysee.com" : "lbry.vern.cc",
    "www.odysee.com" : "lbry.vern.cc",
    "google.com" : "searx.vern.cc",
    "www.google.com" : "searx.vern.cc"
}
