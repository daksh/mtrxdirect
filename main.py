import asyncio
import re
from nio import AsyncClient, MatrixRoom, RoomMessageText, ClientConfig

from config import *

client_config = ClientConfig(store_sync_tokens=True)
client = AsyncClient(hs_url, bot_id, store_path=storage_path, config=client_config)


def get_urls(of):
    regex = r"(?i)\b((?:https?://|www\d{0,3}[.]|[a-z0-9.\-]+[.][a-z]{2,4}/)(?:[^\s()<>]+|\(([^\s()<>]+|(\([^\s()<>]+\)))*\))+(?:\(([^\s()<>]+|(\([^\s()<>]+\)))*\)|[^\s`!()\[\]{};:'\".,<>?«»“”‘’]))"
    urls = re.findall(regex,of)
    return [url_wrapper[0] for url_wrapper in urls]

def purify(urls):
    changed = False
    def purify_solo(url):
        nonlocal changed
        if url.startswith("https://"):
            url_breakdown = url[8::].split("/")
            if url_breakdown[0] in purify_db.keys():
                url_breakdown[0] = purify_db[url_breakdown[0]]
                changed = True
                return "https://"+"/".join(url_breakdown)
            else: return url
        elif url.startswith("http://"):
            url_breakdown = url[7::].split("/")
            if url_breakdown[0] in purify_db.keys():
                url_breakdown[0] = purify_db[url_breakdown[0]]
                changed = True
                return "http://"+"/".join(url_breakdown)
            else: return url
        else:
            url_breakdown = url.split("/")
            if url_breakdown[0] in purify_db.keys():
                url_breakdown[0] = purify_db[url_breakdown[0]]
                changed = True
                return "/".join(url_breakdown)
            else: return url
    purls = [purify_solo(u) for u in urls]
    return purls, changed

async def on_msg(room: MatrixRoom, event: RoomMessageText) -> None:
    if (room.room_id in rooms) or (rooms == "all"):
        if event.sender != bot_id:
            urls = get_urls(event.body)
            if len(urls) != 0:
                purified_urls, changed = purify(urls)
                print(purified_urls, changed)
                if changed:
                    new_msg = event.body
                    for i in range(len(urls)):
                        new_msg = new_msg.replace(urls[i], purified_urls[i])
                    await client.room_send(
                        room_id=room.room_id,
                        message_type="m.room.message",
                        content={"msgtype": "m.text", "body": new_msg}
                    )


async def main() -> None:
    client.add_event_callback(on_msg, RoomMessageText)
    await client.login(password)
    await client.sync_forever(timeout=30000, full_state=True)  # milliseconds


asyncio.get_event_loop().run_until_complete(main())
