# mtrxdirect

A matrix bot to automatically change the URLs of proprietary websites with freedom and privacy respecting front ends


# Getting Started
To get started, just clone this repository to a directory of your choice, with:
```
git clone https://codeberg.org/daksh/mtrxdirect.git
```
Then, move into the newly cloned repository
```
cd mtrxdirect
```
Finally, run the bot with:
```
python main.py # Note: Be sure to configure the bot first, see next section
```

# Configuration
As of now, mtrxdirect is not all that configurable, but a configuration file is found at $REPO_DIR/config.py
* bot_id: the user id of the account that is going to be used
* hs_url: the url of the home server of your matrix account
* rooms: an array of the rooms you want the bot to function in, if you want it to function in all the joined rooms, do `rooms = "all"`
* password: this contains the password for your account. if you dont want to simply put it in a file, you can do `password = input("enter password: ")`, although that way you would have to type it out every time you run the bot
* purify_db: this is the most important configuration, it contains what URLs you would like to correct, and with what you want to correct them. So from the default configuration, we can say youtube.com will be corrected to inv.vern.cc, etc.


# Community
* Join the community over at [Matrix](https://matrix.to/#/#mtrxdirect:vern.cc)
